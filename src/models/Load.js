const mongoose = require('mongoose');

const schema = mongoose.Schema({
    created_by: {
        type: String,
        required: true,
    },
    assigned_to: {
        type: String,
        required: true,
    },
    state: {
        type: String,
        default:"En route to Pick Up",
        required: true,
    },
    status: {
        type: String,
        default:"NEW",
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    payload: {
        type: String,
        required: true,
    },
    pickup_address: {
        type: String,
        required: true,
    },
    delivery_address: {
        type: String,
        required: true,
    },
    dimensions: {
        x: {type: Number},
        y: {type: Number},
        z: {type: Number}
    },
    logs: {
        type: Array,
    },
}, {timestamps: {createdAt: "created_date"}});

let Load = mongoose.model('Load', schema)

module.exports = {
    Load,
};
