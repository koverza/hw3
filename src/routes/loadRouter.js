const express = require('express')
const router = express.Router()
const {postLoads} = require('../services/loadService')
const {authMiddleware} = require('../middleware/authMiddleware')

router.post('/',authMiddleware,postLoads)


module.exports = {
    loadRouter:router,
}
