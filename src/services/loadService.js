const {Load} = require('../models/Load')
const mongoose = require("mongoose");

const postLoads = (req, res, next) => {
    const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
    const load = new Load({
        created_by: req.user.userId,
        assigned_to: req.user.userId,
        status: "NEW",
        state: "En route to Pick Up",
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions,
})
    load.save()
        .then(() => res.json({message: 'Load created successfully'}))
        .catch(err => {
            next(err);
        });
}

module.exports = {
    postLoads,
}
